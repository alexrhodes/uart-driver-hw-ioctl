
/*
*  hw_ioctl_arduino.c - An arduino application to simulate a
*  a custom UART device.
*
*  Copyright (C) 2020, Alex Rhodes <https://www.alexrhodes.io>
* 
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or (at
*  your option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  <https://www.gnu.org/licenses/gpl-3.0.html>
* 
*/

#define SERIAL_TIMEOUT 1000
#define SERIAL_BAUD 115200
#define MSG_BUFF_SIZE 100

const int LED_PIN = LED_BUILTIN; //Board built-in LED GPIO
unsigned char rxBuffer[MSG_BUFF_SIZE]; //Receive message buffer

//Stores the time of the next async status message
unsigned long nextPeriodic; 

//Arduino setup routine
void setup();

//Arduino main loop routine
void loop();

//Receive data from serial port and store for RX state machine
void handleRx(unsigned char * message, unsigned int len);

//RX state machine for incoming commands
void handleMsg(int len);

//Turns the LED on and ACKs the cmd
void ledOn();

//Turns the LED off and ACKs the cmd
void ledOff();

//Adds two numbers and responds with the sum
void add();

//Transmits a status message with the state of the LED
//and the most recent sum
void sendStatus();

//This type enumerates the command IDs
typedef enum CMD_ID
{
    LED_ON,
    LED_OFF,
    ADD,
    STATUS
}CMD_ID;

//Structure for the UART state machine
typedef struct
{
    enum
    {
        RX_CMD,
        RX_LEN,
        RX_DATA,
        HANDLE
    }state;
    CMD_ID curCmd;
    int curLen;
    int mLen;
    unsigned char msg[4];
    int rxIdx;
    int msgIdx;
    int loop;
}rx_state_t;

rx_state_t rxStateVars; //State information for the RX state machine

//This structure represents an async status message
struct status_t
{
  union
  {
    struct{
      unsigned char cmdId;
      unsigned char len;
      unsigned char lastSum;
      unsigned char ledStatus;
    } val;
    unsigned char data[4];
  };
} devStatus;

/*********************************************************/
void ledOn()
{
   //Turn the LED on and update the status structure
   devStatus.val.ledStatus = 1;
   digitalWrite(LED_PIN,HIGH);
   
   //Ack the message
   unsigned char ack[2] = {LED_ON, 0x0};
   Serial.write(ack, 2);
   Serial.flush();
}

/*********************************************************/
void ledOff()
{
   //Turn the LED off and update the status structure
   devStatus.val.ledStatus = 0;
   digitalWrite(LED_PIN, LOW);
   
   //Ack the message
   unsigned char ack[2] = {LED_OFF, 0x0};
   Serial.write(ack, 2);
   Serial.flush();
}

/*********************************************************/
void add(unsigned char a, unsigned char b)
{
  //Add the numbers and update the status structure
  devStatus.val.lastSum = a + b;

  //Ack the message
  unsigned char ack[3] = {ADD, 0x1, devStatus.val.lastSum};
  Serial.write(ack, 3);
  Serial.flush();
}

/*********************************************************/
void sendStatus()
{
  //Send the status structure data
  Serial.write(devStatus.data, 4);
  Serial.flush();
}

/*********************************************************/
void handleMsg(unsigned int len)
{
    //Loop as long as we have data
    rxStateVars.loop = 1;
    while(rxStateVars.loop == 1)
    {
        if(len > 0)
        {
            switch(rxStateVars.state)
            {
                case rx_state_t::RX_CMD:
                {
                    //Clear the message buffer, and receive the command
                    memset(rxStateVars.msg, 0x0, sizeof(rxStateVars.msg));
                    rxStateVars.curCmd = rxBuffer[rxStateVars.rxIdx++];
                    len--;
                    rxStateVars.state = rx_state_t::RX_LEN;
                    if(len == 0)
                    {
                        rxStateVars.loop = 0;
                        break;
                    }
                     
                }
                case rx_state_t::RX_LEN:
                {
                    //Receive the length of the UART command
                    rxStateVars.curLen = rxBuffer[rxStateVars.rxIdx++];
                    rxStateVars.mLen = rxStateVars.curLen;
                    len--;
                    rxStateVars.state = rx_state_t::RX_DATA;
                    if(len == 0 && rxStateVars.curLen != 0)
                    {
                        rxStateVars.loop = 0;
                        break;
                    }
                }
                case rx_state_t::RX_DATA:
                {
                    //Receive the data for the current command
                    int bytes = rxStateVars.mLen;
                    if(bytes > len)
                    {
                        bytes = len;
                    }
                    //Copy the data into the message buffer
                    memcpy(&rxStateVars.msg[rxStateVars.msgIdx], 
                        &rxBuffer[rxStateVars.rxIdx], 
                        bytes);
                    rxStateVars.mLen-= bytes;
                    rxStateVars.msgIdx += bytes;
                    rxStateVars.rxIdx += bytes;
                    len-= bytes;
                    if(rxStateVars.mLen == 0)
                    {
                        rxStateVars.state = rx_state_t::HANDLE;
                    }
                    else
                    {
                        if(len == 0)
                        {
                            rxStateVars.loop = 0;
                            break;
                        }
                    }
                }
                case rx_state_t::HANDLE:
                {
                    //Handle the current message
                    switch(rxStateVars.curCmd)
                    {
                        
                        case LED_ON:
                        {
                          ledOn();
                          break;
                        }
                        case LED_OFF:
                        {
                          ledOff();
                          break;
                        }
                        case ADD:
                        {
                           add(rxStateVars.msg[0], rxStateVars.msg[1]);
                           break;
                        }
                    }

                    //Reset the state machine for the next message
                    rxStateVars.state = rx_state_t::RX_CMD;
                    rxStateVars.msgIdx = 0;
                    if(len == 0)
                    {
                        rxStateVars.loop = 0;
                    }
                    break;
                }
            }
        }
    }
    //Reset the receive buffer index
    rxStateVars.rxIdx = 0;
}

/*********************************************************/
void handleRx()
{
  bool doBreak = false;
  unsigned long startTime = millis();
  unsigned int recvBytes = 0;
  unsigned char c;
  //Loop until data arrives or a timeout occurs
  while(!doBreak)
  {
    int b = Serial.available();
    if(b > 0)
    {
      //If data is available, read
      rxBuffer[recvBytes] = Serial.read();
      recvBytes += b;
    }
    if( (millis() - startTime) > SERIAL_TIMEOUT)
    {
      //Timed out waiting for data
      doBreak = true;
    }
    else if(recvBytes == MSG_BUFF_SIZE)
    {
      //Break if the rx buffer is full
      doBreak = true;
    }
  }
  if(recvBytes > 0)
  {
    //If data was received, pass it to the state machine
    handleMsg(recvBytes);
  }
}

/*********************************************************/
void setup() {
  //Start serial, set the LED GPIO to an output,
  //and initialize the status structure
  Serial.begin(SERIAL_BAUD);
  pinMode(LED_PIN, OUTPUT);
  devStatus.val.ledStatus = LOW;
  devStatus.val.lastSum = 0;
  nextPeriodic = millis() + random(1000,5001);
}

/*********************************************************/
void loop() {
  devStatus.val.cmdId = (unsigned char)STATUS;
  devStatus.val.len = 2;
  handleRx();

  //Simulate some random async messages
  unsigned long now = millis();
  if(now > nextPeriodic)
  {
    sendStatus();
    nextPeriodic = millis() + random(1000,5001);
  }
}