
/*
*  hw_ioctl.h - Defines IOCTL calls on a 
*  device driver for a custom UART device
*
*  Copyright (C) 2020, Alex Rhodes <https://www.alexrhodes.io>
* 
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or (at
*  your option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  <https://www.gnu.org/licenses/gpl-3.0.html>
* 
*/
#ifndef HW_IOCTL_H
#define HW_IOCTL_H

#include <linux/ioctl.h>

//This type enumerates the IOCTL numbers
typedef enum 
{
    LED_ON_IOCTL_NUM = 0xA,
    LED_OFF_IOCTL_NUM,
    ADD_IOCTL_NUM,
    GET_STATUS_IOCTL_NUM
} HW_IOCTL_ID;

//This type defines the data structure for the status
//IOCTL call
typedef struct
{
    char lastSum;
    char ledStatus;
} hw_ioctl_status_t;

//This type defines the data structure for the add
//IOCTL call
typedef struct
{
    char numA;
    char numB;
    char sum;
} hw_ioctl_add_t;

//The IOCTL magic for the driver
#define HW_IOCTL_MAGIC 0x249

//LED on IOCTL
#define HW_IOCTL_LED_ON _IO(HW_IOCTL_MAGIC, LED_ON_IOCTL_NUM)

//LED off IOCTL
#define HW_IOCTL_LED_OFF _IO(HW_IOCTL_MAGIC, LED_OFF_IOCTL_NUM)

//Add IOCTL
#define HW_IOCTL_ADD _IOWR(HW_IOCTL_MAGIC, ADD_IOCTL_NUM, hw_ioctl_add_t)

//Status IOCTL
#define HW_IOCTL_GET_STS _IOR(HW_IOCTL_MAGIC, GET_STATUS_IOCTL_NUM, hw_ioctl_status_t)
#endif