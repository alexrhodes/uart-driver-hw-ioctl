/*
*  hw_ioctl.c - A driver to interface with a custom, external 
*  UART device for the Beaglebone Black
*
*  Copyright (C) 2020, Alex Rhodes <https://www.alexrhodes.io>
* 
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or (at
*  your option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  <https://www.gnu.org/licenses/gpl-3.0.html>
* 
*/

#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/serial_reg.h>
#include <linux/of.h>
#include <linux/io.h>
#include <linux/pm_runtime.h>
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/irqreturn.h>
#include <linux/wait.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/semaphore.h>
#include <linux/jiffies.h>
#include "hw_ioctl.h"

#define BUFF_SIZE 512
#define TIMEOUT_SEC 5 //Timeout for command ACK message

//Command ID enumeration
typedef enum CMD_ID_NUM
{
    LED_ON,
    LED_OFF,
    ADD,
    STATUS
}CMD_ID_NUM;

typedef unsigned char CMD_ID;

//LED on and off command
typedef struct 
{
    struct semaphore sem;
    union
    {
        struct
        {
            CMD_ID cmdId;
            unsigned char length;
        } fields;
        unsigned char data[2];
    } cmd;
    union
    {
        struct
        {
            CMD_ID cmdId;
            unsigned char length;
        } fields;
        unsigned char data[2];
    } resp;
}led_t;

//Add two numbers command
typedef struct 
{
    struct semaphore sem;
    union
    {
        struct
        {
            CMD_ID cmdId;
            unsigned char length;
            unsigned char op1;
            unsigned char op2;
        } fields;
        unsigned char data[4];
    } cmd;
    union
    {
        struct
        {
            CMD_ID cmdId;
            unsigned char length;
            unsigned char sum;
        } fields;
        unsigned char data[3];
    } resp;
} add_t;

//Async status messages
typedef struct 
{
    struct semaphore sem;
    union
    {
        struct
        {
            CMD_ID cmdId;
            unsigned char length;
            unsigned char lastSum;
            unsigned char ledState;
        } fields;
        unsigned char data[4];
    } resp;
    int hasRx
} status_t;

//Command list structure
typedef struct
{
    led_t ledOnCmd;
    led_t ledOffCmd;
    add_t addCmd;
    status_t statusMsg;
}cmd_list_t;


//Structure for the UART state machine
typedef struct
{
    enum
    {
        RX_CMD,
        RX_LEN,
        RX_DATA,
        HANDLE
    }state;
    CMD_ID curCmd;
    int curLen;
    int mLen;
    unsigned char msg[4];
    int rxIdx;
    int msgIdx;
    int loop;
}rx_state_t;


//Serial device struct
static struct hw_serial_dev
{
    void __iomem *regs;
    struct miscdevice mDev;
    int irq;
    unsigned long irqFlags;
    spinlock_t lock;
    cmd_list_t cmdList;
    rx_state_t rxStateVars;
    unsigned char rxBuffer[BUFF_SIZE];
};


//Initialize the device commands
static void hw_init_commands(struct hw_serial_dev * dev);

//Driver probe routine
static int hw_probe(struct platform_device *pdev);

//Driver remove routine
static int hw_remove(struct platform_device *pdev);

//FOPS open
static int hw_open(struct inode *inode, struct file *file);

//FOPS close
static int hw_close(struct inode *inodep, struct file *filp);

//Routine to read from serial device registers
static unsigned int reg_read(struct hw_serial_dev *dev, int offset);

//Routine to write to serial device registers
static void reg_write(struct hw_serial_dev *dev, int val, int offset);

//Routine to write a character to the seriald device
static void write_char(struct hw_serial_dev *dev, char test);

//UART rx state machine
static void handle_rx(struct hw_serial_dev * dev, unsigned int len);

//Interrupt handler
static irqreturn_t irqHandler(int irq, void *devid);

//Transmit command and wait for a response
static int tx_and_wait(struct hw_serial_dev *dev, CMD_ID cmdId);

//Handle IOCTL calls to this driver
static long unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long argp);

//Device id struct
static struct of_device_id hw_match_table[] =
    {
        {
            .compatible = "serial",
        },
};

//File operations struct
static const struct file_operations hw_fops = {
    .owner = THIS_MODULE,
    .open = hw_open,
    .release = hw_close,
    .llseek = no_llseek,
    .unlocked_ioctl = unlocked_ioctl
};

//Platform driver structure
static struct platform_driver hw_plat_driver = {
    .driver = {
        .name = "serial",
        .owner = THIS_MODULE,
        .of_match_table = hw_match_table},
    .probe = hw_probe,
    .remove = hw_remove
};

/*********************************************************/
static int hw_open(struct inode *inode, struct file *file)
{
    return 0;
}
/*********************************************************/
static int hw_close(struct inode *inodep, struct file *filp)
{
    return 0;
}

/*********************************************************/
static unsigned int reg_read(struct hw_serial_dev *dev, int offset)
{
    spin_lock_irqsave(&dev->lock, dev->irqFlags);
    unsigned int ret = ioread32(dev->regs + (4 * offset));
    spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
    return ret;
}

/*********************************************************/
static void reg_write(struct hw_serial_dev *dev, int val, int offset)
{
    spin_lock_irqsave(&dev->lock, dev->irqFlags);
    iowrite32(val, dev->regs + (4 * offset));
    spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
    return;
}

/*********************************************************/
static void write_char(struct hw_serial_dev *dev, char c)
{
    unsigned int lsr = reg_read(dev, UART_LSR);
    while (1)
    {
        if (lsr & UART_LSR_THRE)
        {
            break;
        }
        lsr = reg_read(dev, UART_LSR);
    }
    reg_write(dev, c, UART_TX);
}


/*********************************************************/
static void handle_rx(struct hw_serial_dev * dev, unsigned int len)
{
    //Loop as long as we have data
    dev->rxStateVars.loop = 1;
    while(dev->rxStateVars.loop == 1)
    {
        if(len > 0)
        {
            switch(dev->rxStateVars.state)
            {
                case RX_CMD:
                {
                    //Clear the message buffer, and receive the command
                    memset(dev->rxStateVars.msg, 0x0, sizeof(dev->rxStateVars.msg));
                    dev->rxStateVars.curCmd = dev->rxBuffer[dev->rxStateVars.rxIdx++];
                    len--;
                    dev->rxStateVars.state = RX_LEN;
                    if(len == 0)
                    {
                        //Break if no more data is available
                        dev->rxStateVars.loop = 0;
                        break;
                    }
                     
                }
                case RX_LEN:
                {
                    //Receive the length of the UART command
                    dev->rxStateVars.curLen = dev->rxBuffer[dev->rxStateVars.rxIdx++];
                    dev->rxStateVars.mLen = dev->rxStateVars.curLen;
                    len--;
                    dev->rxStateVars.state = RX_DATA;
                    if(len == 0 && dev->rxStateVars.curLen != 0)
                    {
                        //Break if no more data is available
                        dev->rxStateVars.loop = 0;
                        break;
                    }
                }
                case RX_DATA:
                {
                    //Receive the data for the current command
                    int bytes = dev->rxStateVars.mLen;
                    if(bytes > len)
                    {
                        bytes = len;
                    }
                    //Copy the data into the message buffer
                    memcpy(&dev->rxStateVars.msg[dev->rxStateVars.msgIdx], 
                        &dev->rxBuffer[dev->rxStateVars.rxIdx], 
                        bytes);
                    dev->rxStateVars.mLen-= bytes;
                    dev->rxStateVars.msgIdx += bytes;
                    dev->rxStateVars.rxIdx += bytes;
                    len-= bytes;
                    if(dev->rxStateVars.mLen == 0)
                    {
                        //If the whole message has been received, 
                        //handle it.
                        dev->rxStateVars.state = HANDLE;
                    }
                    else
                    {
                        if(len == 0)
                        {
                            //Break if no more data is available
                            dev->rxStateVars.loop = 0;
                            break;
                        }
                    }
                }
                case HANDLE:
                {
                    //Handle the current message
                    switch(dev->rxStateVars.curCmd)
                    {
                        
                        case LED_ON:
                        {
                            up(&dev->cmdList.ledOnCmd.sem);
                            break;
                        }
                        case LED_OFF:
                        {
                            up(&dev->cmdList.ledOffCmd.sem);
                            break;
                        }
                        case ADD:
                        {
                            spin_lock_irqsave(&dev->lock, dev->irqFlags);
                            //Copy the command response into the response object
                            dev->cmdList.addCmd.resp.fields.cmdId = ADD;
                            dev->cmdList.addCmd.resp.fields.length = 1;
                            dev->cmdList.addCmd.resp.fields.sum = dev->rxStateVars.msg[0];
                            spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
                            up(&dev->cmdList.addCmd.sem);
                            break;
                        }
                        case STATUS:
                        {
                            spin_lock_irqsave(&dev->lock, dev->irqFlags);
                            //Copy the command response into the response object
                            dev->cmdList.statusMsg.resp.fields.cmdId = STATUS;
                            dev->cmdList.statusMsg.resp.fields.length = 2;
                            dev->cmdList.statusMsg.resp.fields.lastSum = dev->rxStateVars.msg[0];
                            dev->cmdList.statusMsg.resp.fields.ledState = dev->rxStateVars.msg[1];
                            dev->cmdList.statusMsg.hasRx = 1;
                            spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
                            break;
                        }

                    }

                    //Reset the state machine for the next message
                    dev->rxStateVars.state = RX_CMD;
                    dev->rxStateVars.msgIdx = 0;
                    if(len == 0)
                    {
                        //Break if no more data is available
                        dev->rxStateVars.loop = 0;
                    }
                    break;
                }
            }
        }
    }
    //Reset the receive buffer index
    dev->rxStateVars.rxIdx = 0;
}

/*********************************************************/
static irqreturn_t irqHandler(int irq, void *d)
{
    struct hw_serial_dev *dev = d;
    int count = 0;
    do 
    {
        char recv = reg_read(dev, UART_RX);
        dev->rxBuffer[count++] = recv;
    }
    while (reg_read(dev, UART_LSR) & UART_LSR_DR);
    
    handle_rx(dev, count);

    return IRQ_HANDLED;
}

/*********************************************************/
static int tx_and_wait(struct hw_serial_dev *dev, CMD_ID cmdId)
{
    unsigned char * ptr;
    unsigned int length;
    struct semaphore * sem;
    
    switch(cmdId)
    {
        case LED_ON:
        {
            ptr = dev->cmdList.ledOnCmd.cmd.data;
            length = sizeof(dev->cmdList.ledOnCmd.cmd.data);
            sem = &dev->cmdList.ledOnCmd.sem;
            break;
        }
        case LED_OFF:
        {
            ptr = dev->cmdList.ledOffCmd.cmd.data;
            length = sizeof(dev->cmdList.ledOffCmd.cmd.data);
            sem = &dev->cmdList.ledOffCmd.sem;
            break;
        }
        case ADD:
        {
            ptr = dev->cmdList.addCmd.cmd.data;
            length = sizeof(dev->cmdList.addCmd.cmd.data);
            sem = &dev->cmdList.addCmd.sem;
            break;
        }
        default: return -EINVAL;
    }
    //Send the data bytes
    int i = 0;
    for(i = 0; i < length; i++)
    {
        write_char(dev, ptr[i]);
    }
    //Wait for TIMEOUT_SEC for the ACK semaphore
    unsigned long timeout = jiffies + TIMEOUT_SEC * HZ;
    while(jiffies <= timeout)
    {
        if(down_trylock(sem) == 0)
        {
            //Response received
            return 0;
        }
    }
    //Timed out waiting for response
    return -ETIMEDOUT;
}

/*********************************************************/
long unlocked_ioctl(struct file *file, unsigned int cmd, unsigned long argp)
{
    struct miscdevice *mdev = (struct miscdevice *)file->private_data;
    struct hw_serial_dev *dev = container_of(mdev, struct hw_serial_dev, mDev);

    switch(cmd)
    {
        case LED_ON_IOCTL_NUM:
        {
            //Turn on the LED
            return (long)tx_and_wait(dev, LED_ON);
        }
        case LED_OFF_IOCTL_NUM:
        {
            //Turn off the LED
            return (long)tx_and_wait(dev, LED_OFF);
        }
        case ADD_IOCTL_NUM:
        {
            //Add two numbers
            hw_ioctl_add_t * user = (hw_ioctl_add_t*) argp;
            hw_ioctl_add_t kuser;
            if( copy_from_user(&kuser, user, sizeof(hw_ioctl_add_t) ) )
            {
                return -EFAULT;
            }
            dev->cmdList.addCmd.cmd.fields.op1 = kuser.numA;
            dev->cmdList.addCmd.cmd.fields.op2 = kuser.numB;
            if(tx_and_wait(dev,  ADD) != 0)
            {
                //Timed out waiting for the response
                return -ETIMEDOUT;
            }
            spin_lock_irqsave(&dev->lock, dev->irqFlags);
            kuser.sum = dev->cmdList.addCmd.resp.fields.sum;
            spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
            if( copy_to_user(user, &kuser, sizeof(hw_ioctl_add_t) ) )
            {
                return -EFAULT;
            }
            return 0;
        }
        case GET_STATUS_IOCTL_NUM:
        {
            hw_ioctl_status_t * user = (hw_ioctl_status_t*) argp;
            hw_ioctl_status_t kuser;
            spin_lock_irqsave(&dev->lock, dev->irqFlags);
            kuser.lastSum = dev->cmdList.statusMsg.resp.fields.lastSum;
            kuser.ledStatus = dev->cmdList.statusMsg.resp.fields.ledState;
            int rx = dev->cmdList.statusMsg.hasRx;
            spin_unlock_irqrestore(&dev->lock, dev->irqFlags);
            if(rx == 0)
            {
                //We haven't received a status message yet
                return -ENODATA;
            }
            if( copy_to_user(user, &kuser, sizeof(hw_ioctl_status_t) ) )
            {
                return -EFAULT;
            }
            return 0;
        }
        default:
            return -EINVAL;
    }
}

/*********************************************************/
static void hw_init_commands(struct hw_serial_dev * dev)
{
    //LED on
    dev->cmdList.ledOnCmd.cmd.fields.cmdId = LED_ON;
    dev->cmdList.ledOnCmd.cmd.fields.length = 0x0;
    sema_init(&dev->cmdList.ledOnCmd.sem, 0);

    //LED off
    dev->cmdList.ledOffCmd.cmd.fields.cmdId = LED_OFF;
    dev->cmdList.ledOffCmd.cmd.fields.length = 0x0;
    sema_init(&dev->cmdList.ledOffCmd.sem, 0);

    //Add command
    dev->cmdList.addCmd.cmd.fields.cmdId = ADD;
    dev->cmdList.addCmd.cmd.fields.length = 2;
    dev->cmdList.addCmd.cmd.fields.op1 = 0;
    dev->cmdList.addCmd.cmd.fields.op1 = 0;
    sema_init(&dev->cmdList.addCmd.sem, 0);

    //Status message
    dev->cmdList.statusMsg.hasRx = 0;
    sema_init(&dev->cmdList.statusMsg.sem, 1);
}

/*********************************************************/
static int hw_probe(struct platform_device *pdev)
{
    struct resource *res;
    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

    if (!res)
    {
        pr_err("%s: platform_get_resource returned NULL\n", __func__);
        return -EINVAL;
    }

    struct hw_serial_dev *dev = devm_kzalloc(&pdev->dev, sizeof(struct hw_serial_dev), GFP_KERNEL);
    if (!dev)
    {
        pr_err("%s: devm_kzalloc returned NULL\n", __func__);
        return -ENOMEM;
    }
    
    hw_init_commands(dev);

    dev->regs = devm_ioremap_resource(&pdev->dev, res);

    if (IS_ERR(dev->regs))
    {
        dev_err(&pdev->dev, "%s: Can not remap registers\n", __func__);
        return PTR_ERR(dev->regs);
    }
    
    //Configure interrupts
    dev->irq = platform_get_irq(pdev, 0);
	if (dev->irq < 0) {
		dev_err(&pdev->dev, "%s: unable to get IRQ\n", __func__);
		return dev->irq;
	}
	int ret = devm_request_irq(&pdev->dev, dev->irq, irqHandler, 0, "hw_serial", dev);
	if (ret < 0) 
    {
		dev_err(&pdev->dev, "%s: unable to request IRQ %d (%d)\n", __func__, dev->irq, ret);
		return ret;
	}

    //Enable power management runtime
    pm_runtime_enable(&pdev->dev);
    pm_runtime_get_sync(&pdev->dev);

    //Configure the UART device
    unsigned int baud_divisor;
    unsigned int uartclk;

    of_property_read_u32(pdev->dev.of_node, "clock-frequency", &uartclk);

    baud_divisor = uartclk / 16 / 115200;

    reg_write(dev, UART_OMAP_MDR1_DISABLE, UART_OMAP_MDR1);
    reg_write(dev, 0x00, UART_LCR);
    reg_write(dev, UART_LCR_DLAB, UART_LCR);
    reg_write(dev, baud_divisor & 0xff, UART_DLL);
    reg_write(dev, (baud_divisor >> 8) & 0xff, UART_DLM);
    reg_write(dev, UART_LCR_WLEN8, UART_LCR);
    reg_write(dev, UART_FCR_CLEAR_RCVR | UART_FCR_CLEAR_XMIT | UART_FCR_ENABLE_FIFO, UART_FCR);

    reg_write(dev, UART_OMAP_MDR1_16X_MODE, UART_OMAP_MDR1);

    //Initialize and register a misc device
    dev->mDev.minor = MISC_DYNAMIC_MINOR;
    dev->mDev.name = devm_kasprintf(&pdev->dev, GFP_KERNEL, "hw_serial-%x", res->start);
    dev->mDev.fops = &hw_fops;

    int error = misc_register(&dev->mDev);
    if (error)
    {
        pr_err("%s: misc register failed.", __func__);
        return error;
    }

    dev_set_drvdata(&pdev->dev, dev);

    //Enable RX interrupt
    reg_write(dev, UART_IER_RDI, UART_IER);
    return 0;
}

/*********************************************************/
static int hw_remove(struct platform_device *pdev)
{
    pm_runtime_disable(&pdev->dev);
    struct hw_serial_dev *dev = dev_get_drvdata(&pdev->dev);
    misc_deregister(&dev->mDev);
    return 0;
}

MODULE_AUTHOR("Alex Rhodes");
MODULE_LICENSE("GPL");

//Register the platform driver
module_platform_driver(hw_plat_driver);

