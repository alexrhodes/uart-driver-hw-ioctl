
/*
*  hw_ioctl_user.c - A userspace application to make
*  IOCTL calls on a device driver for a custom UART device
*
*  Copyright (C) 2020, Alex Rhodes <https://www.alexrhodes.io>
* 
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or (at
*  your option) any later version.
*
*  This program is distributed in the hope that it will be useful, but
*  WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
*  <https://www.gnu.org/licenses/gpl-3.0.html>
* 
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include "hw_ioctl.h"
#include "unistd.h"

//Turn the LED on or off
void ioctl_led(int fd, int on)
{
    int ret;
    if (on == 1)
    {
        //Call the "led on" IOCTL
        ret = ioctl(fd, LED_ON_IOCTL_NUM, 0);
        if (ret != 0)
        {
            printf("Led on failed: %d\n", ret);
        }
        else
        {
            printf("Led on success.\n");
        }
    }
    else
    {
        //Call the "led off" IOCTL
        ret = ioctl(fd, LED_OFF_IOCTL_NUM, 0);
        if (ret != 0)
        {
            printf("Led off failed: %d\n", ret);
        }
        else
        {
            printf("Led off success.\n");
        }
    }
}

//Add two numbers
void ioctl_add(int fd, unsigned char numA, unsigned char numB)
{
    //Populate the command structure with the values to add
    hw_ioctl_add_t add;
    add.numA = numA;
    add.numB = numB;

    //Call the "add" IOCTL
    int ret = ioctl(fd, ADD_IOCTL_NUM, &add);

    if (ret != 0)
    {
        printf("Add failed: %d\n", ret);
    }
    else
    {
        //Print the SUM returned by teh device
        printf("Add success, sum: %d.\n", add.sum);
    }
}

//Query the latest asynchronous status message
void ioctl_get_status(int fd)
{
    //Call the IOCTL with the status structure
    hw_ioctl_status_t stat;
    int ret = ioctl(fd, GET_STATUS_IOCTL_NUM, &stat);
    if(ret != 0)
    {
        printf("Status failed: %d\n", ret);
    }
    else
    {
        //Print the information sent by the device
        printf("Status success, last sum: %d led state: %d\n",
               stat.lastSum, stat.ledStatus);
    }
    
}

//Main entry point for the program
void main()
{
    //Open the driver's device entry file
    int fd;

    fd = open("/dev/hw_serial-48024000", O_RDWR);

    if (fd < 0)
    {
        printf("File open error\n");
    }

    //Turn the LED on
    ioctl_led(fd, 1);

    //Wait a little while for an asynchronous status message
    sleep(4);

    //Query the latest status update
    ioctl_get_status(fd);

    //Blink the LED
    for(int i = 0; i < 3; i++)
    {
        //Turn the LED ON
        ioctl_led(fd, 1);
        sleep(1);

        //Turn the LED off
        ioctl_led(fd, 0);
        sleep(1);

    }

    //Add two numbers
    ioctl_add(fd, 5, 7);

    //Query the status again
    ioctl_get_status(fd);

    //Close the driver's device file
    close(fd);
}